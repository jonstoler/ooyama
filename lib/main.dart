import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:chewie/chewie.dart';
import 'package:video_player/video_player.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Games Done Quick Archives',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: ArchiveList());
  }
}

class ArchiveListState extends State {
  var listWidgets = <Widget>[];

  @override
  void initState() {
    super.initState();
    _getMarathons();
  }

  _getMarathons() async {
    List<Widget> widgets = [];
    var userList = await fetchAndParseMarathons();
    userList.forEach((marathon) => widgets.add(new ListTile(
        title: new Text(marathon.name),
        subtitle: new Text(marathon.date),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => MarathonScreen(marathon: marathon)));
        })));

    setState(() => this.listWidgets = widgets);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text("Games Done Quick Archives"),
        ),
        body: new ListView(children: listWidgets));
  }
}

class ArchiveList extends StatefulWidget {
  @override
  ArchiveListState createState() => ArchiveListState();
}

class Marathon {
  final String name;
  final String date;
  final String archiveURL;

  Marathon.fromJsonMap(Map jsonMap)
      : name = jsonMap["name"],
        date = jsonMap["date"],
        archiveURL = jsonMap["archiveURL"];
}

Future<List<Marathon>> fetchAndParseMarathons() async {
  var res = await http.get("https://gitlab.com/jonstoler/ooyama/raw/main/gdq.json");
  var jsonStr = res.body;

  var parsedMarathonList = jsonDecode(jsonStr);
  var marathonList = <Marathon>[];
  parsedMarathonList.forEach((parsedMarathon) {
    marathonList.add(new Marathon.fromJsonMap(parsedMarathon));
  });

  return marathonList;
}

class MarathonScreen extends StatelessWidget {
  final Marathon marathon;

  MarathonScreen({Key key, @required this.marathon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(marathon.name),
        ),
        body: RunList(marathon: marathon.archiveURL));
  }
}

class RunListState extends State {
  var listWidgets = <Widget>[];
  var marathon;

  RunListState({@required this.marathon}) : super();

  @override
  void initState() {
    super.initState();
    _getRuns();
  }

  _getRuns() async {
    List<Widget> widgets = [];
    var userList = await fetchAndParseRuns(this.marathon);
    userList.forEach((run) => widgets.add(new ListTile(
        title: new Text(run.name),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => VideoScreen(videoURL: "https://archive.org/download/$marathon/${run.fullName}")));
        })));

    setState(() => this.listWidgets = widgets);
  }

  @override
  Widget build(BuildContext context) {
    return new ListView(children: listWidgets);
  }
}

class RunList extends StatefulWidget {
  final marathon;

  RunList({@required this.marathon}) : super();

  @override
  RunListState createState() => RunListState(marathon: this.marathon);
}

class Run {
  final String name;
  final String fullName;
  final String fullURL;

  Run.fromJsonMap(Map jsonMap)
      : fullName = jsonMap["name"],
        name = jsonMap["name"].toString().split("_").last.split(".").first,
        fullURL = jsonMap["name"];
}

Future<List<Run>> fetchAndParseRuns(String marathon) async {
  var res = await http.get("https://archive.org/metadata/$marathon");
  var jsonStr = res.body;

  var parsedMarathonList = jsonDecode(jsonStr);
  var marathonList = <Run>[];
  parsedMarathonList["files"].forEach((parsedMarathon) {
    if (parsedMarathon["source"] != "original") return;
    marathonList.add(new Run.fromJsonMap(parsedMarathon));
  });

  return marathonList;
}

class VideoScreen extends StatelessWidget {
  final String videoURL;
  VideoPlayerController videoPlayerController;
  ChewieController chewieController;

  VideoScreen({Key key, @required this.videoURL}) {
    this.videoPlayerController = VideoPlayerController.network(videoURL);
    this.chewieController = ChewieController(
      videoPlayerController: videoPlayerController,
      autoPlay: true,
      fullScreenByDefault: true,
      deviceOrientationsAfterFullScreen: [DeviceOrientation.landscapeRight, DeviceOrientation.landscapeLeft]
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("watch"),
        ),
        body: Chewie(
          controller: this.chewieController
        )
    );
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    chewieController.dispose();
  }
}

